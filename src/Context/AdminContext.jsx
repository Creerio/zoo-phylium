import React from "react";

async function getUserName() {
    let name = "";
    await fetch('/api/isadmin', {
        method: 'GET',
    })
        .then(function (response) {
            return response.text();
        })
        .then(function (data) {
            //var userid = JSON.parse(data);
            name = data;
            return data;
        })
    return name
}

let UserContext;
export default UserContext = React.createContext(await getUserName());

import React from "react";
import './AnimalMap.css'
import {Link} from "react-router-dom";

export default class AnimalsMap extends React.Component {
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            animals: [],
        }
    }
    loadMapAnimalsEvent=()=> {
        fetch('/api/pos')
            .then((res) => res.json())
            .then((eventResponse) => {
                // on met à jour l'état de notre composant
                // ce qui forcera son rendu, donc l'appel à la méthode render
                this.setState({animals: eventResponse})
            })
    }
    componentDidMount() {
        // Appel vers notre serveur
        this.loadMapAnimalsEvent();
    }

    render(){
        return(
            <div id="animalMap" className="text-center map-wrapper">
                <img src="/images/zooMap.png" className="img-fluid" alt="mapZoo"/>
                { this.state.animals.map(animal=>{
                    return(
                        <div style={{left: animal.posX + '%', top: animal.posY + '%'}}>
                            <img className="svg-icon-animal-pos" src="/logo/arrow-drop-down-fill.svg" style={{left: animal.posX + '%', top: (animal.posY + 4) + '%'}} alt=""/>
                            <Link to={`/Animals/${animal.id}`}>
                                <img src={"/" + animal.imageUrl} className="map-animal-thumbnail rounded-circle" style={{left: animal.posX + '%', top: animal.posY + '%'}} alt=""/>
                            </Link>
                        </div>

                    )
                })
                }
        </div> )
    }


}

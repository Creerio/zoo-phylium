import React from "react";
import './Accueil.css'
import AnimalMap from '../AnimalMap/AnimalMap';

export default class Accueil extends React.Component {
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            animals: []
        }
    }

    render(){
        return(
            <div id="accueil">
                <div className="banner-wrapper">
                    <img src="/images/banner.jpg" className="banner-img" alt="banner"/>
                    <div className="banner-txt">
                        <h1>Bienvenue au Zoo Phyllium!</h1>
                        <h2>Le premier zoo-restaurant du monde</h2>
                    </div>
                </div>

                <div className="container">
                    <div className="row group-separation">
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                        <img src="/images/top5zoos3.png" className="logo-top5" alt="logotop5"/>
                    </div>
                    <div className="row group-separation">
                        <h3>On est pas mieux que les autres zoos, <strong>mais au moins on assume.</strong></h3><br/>
                        <p>
                            Malnutrition, élevage en masse, non respect des normes environnementales : on accumule les
                            défauts, mais au moins on est transparent sur nos valeurs! Notre but est d'éduquer nos visiteurs
                            sur les différentes cultures du monde... d'un point de vue gastronomique. Nous sommes peut-être l'un
                            des pires zoo, mais au moins nous sommes toujours en lice pour devenir l'un des meilleurs restaurants
                            du monde.
                        </p>
                    </div>
                    <div className="row group-separation">
                        <div className="col-lg-5 col-md-12 col-sm-12">
                            <div id="carouselExampleCaptions" className="carousel slide carousel-fade" data-bs-ride="carousel">
                                <div className="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                            className="active" aria-current="true" aria-label="Slide 1"/>
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                            aria-label="Slide 2"/>
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                            aria-label="Slide 3"/>
                                </div>
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img src="/images/slide1.jpg" className="d-block w-100" alt="Viande1" data-bs-interval="2000"/>
                                    </div>
                                    <div className="carousel-item">
                                        <img src="/images/slide2.jpg" className="d-block w-100" alt="Viande2" data-bs-interval="2000"/>
                                    </div>
                                    <div className="carousel-item">
                                        <img src="/images/slide3.jpg" className="d-block w-100" alt="Viande3" data-bs-interval="2000"/>
                                    </div>
                                </div>
                                <button className="carousel-control-prev" type="button"
                                        data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Previous</span>
                                </button>
                                <button className="carousel-control-next" type="button"
                                        data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-7 col-md-12 col-sm-12">
                            <h3>Le seul zoo où on aime <strong>vraiment</strong> les animaux ♥</h3>
                            <p>
                                Venez goûter au goût de toutes les régions du monde... de manière assez littérale!<br/>
                                Nos différents restaurants dispersés au sein du parc vous feront vous lécher vos babines à
                                l'odeur de notre grillade de lion, de rôti de paon blanc, de mets plus raffinés comme
                                un tartare de girafe, un dessert parfumé à la graisse de phoque ou bien d'une assiette
                                d'insectes séchés en guise d'apéritif.<br/><br/>
                                Vous pourrez profiter d'un bon repas près de vos animaux préférés dans nos restaurants
                                réputés dans toute la France:
                                <ul>
                                    <li><strong>Grrrr!</strong> (elephant, girafe, gazelle, zèbre, lion)</li>
                                    <li><strong>Infeste</strong> (insectes, lézards, serpents, tortues, araignées, caméléon)</li>
                                    <li><strong>Mille Mets sous les Mers</strong> (loutre, phoque, dauphin)</li>
                                    <li><strong>Golden Wing</strong> (oiseaux)</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div><br/>

                <div className="timer-wrapper">
                    <div className="container">
                        <h2>Réouverture prochaine de la billetterie!</h2>
                        <p>
                            Le Zoo Phyllium est fermé pour cause d'inspections concernant l'hygiène
                            de ses restaurants ainsi que les conditions de travail de son personnel.<br/>
                            Nous respectons les droits et l'éthique du travail ♥ et avons hâte de vous revoir le 20 janvier 2076 prochain!
                        </p><br/>

                        <div id="billetterie" className="row group-separation">
                            <div className="col-lg-4">
                                <div className="card">
                                    <div className="card-header">Pass <strong>Petite Faim</strong> (enfant de moins de 16ans)</div>
                                    <div className="card-body">
                                        Entrée 1 jour pour un enfant.
                                    </div>
                                    <div className="card-footer">
                                        <button type="button" className="btn btn-secondary btn-fright" disabled>49.99€</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="card">
                                    <div className="card-header">Pass <strong>Plus Chair</strong> (adulte)</div>
                                    <div className="card-body">
                                        Entrée 1 jour pour un adulte.
                                    </div>
                                    <div className="card-footer">
                                        <button type="button" className="btn btn-secondary btn-fright" disabled>89.99€</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="card">
                                    <div className="card-header">Pass <strong>Famine</strong></div>
                                    <div className="card-body">
                                        Entrée 1 jour pour deux enfants et deux adultes.
                                    </div>
                                    <div className="card-footer">
                                        <button type="button" className="btn btn-secondary btn-fright" disabled>199.99€</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="row group-separation">
                        <h3><strong>Plan du site</strong></h3><br/>
                        <p>
                            Vous trouverez sur ce plan l'emplacement de nos délicieux animaux mais aussi
                            l'emplacement de nos restaurants raffinés.
                            Nous avons hâte de vous y retrouver prochainement!<br/>Bon appétit!
                        </p>
                    </div>
                </div>
                <AnimalMap/>
            </div>
        )
    }


}

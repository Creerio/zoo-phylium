import React from "react";
import {Link} from "react-router-dom";
import Menu from "../Menu/Menu";
import UserContext from "../../Context/UserContext";

export default class FavoriteAnimals extends React.Component {
    static contextType = Menu.contextType;
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            animals: [],
            id : [],
        }
    }
    loadAnimalsEvent=()=> {
        fetch('/api/getfavs')
            .then((res) => res.json())
            .then((eventResponse) => {
                // on met à jour l'état de notre composant
                // ce qui forcera son rendu, donc l'appel à la méthode render
                this.setState({animals: eventResponse})
            })
    }

    componentDidMount() {
        // Appel vers notre serveur
        this.loadAnimalsEvent();
    }

    async addDelFav(id) {
        await fetch('/api/adddelfav',{
            method:'POST',
            body: JSON.stringify({
                animalId : id,
            }),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
    }

    async removeFav(id) {
        await this.addDelFav(id);
        this.loadAnimalsEvent();
    }

    render(){

        return(<div className="container">
            <div className="row group-separation">
                {
                    this.state.animals.map(animal=>{
                        return(
                            <div id={`animal-card-${animal.id}`} className="col-3 d-flex align-items-stretch group-separation">
                                <div className="card">
                                    <div className="card-header">
                                        {animal.name}
                                    </div>
                                    <div className="card-img-top">
                                        <img src={animal.imageUrl} className="img-fluid card-animal-thumbnail"></img>
                                    </div>
                                    <div className="card-footer card-animal-footer">
                                        <Link to={`/Animals/${animal.id}`}>
                                            <button type="button" className="btn btn-primary btn-fright">Voir +</button>
                                        </Link>
                                        <UserContext.Consumer>
                                            {userName => userName !== "" ?
                                                <button id={animal.id} name="animalId" type={"submit"} value={animal.id} onClick ={(e)=>this.removeFav(animal.id)} className="btn btn-primary btn-fright">
                                                    <img id={"fav-" + animal.id} className="svg-icon-white" src="/logo/star-fill.svg" alt="isFav"/>
                                                </button>
                                                :
                                                null
                                            }
                                        </UserContext.Consumer>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>)
    }

}
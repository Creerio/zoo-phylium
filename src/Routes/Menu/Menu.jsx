import React from "react";
import {Link, Outlet} from "react-router-dom";
import UserContext from "../../Context/UserContext"
import AdminContext from "../../Context/AdminContext";

export default class Menu extends React.Component{
    static contextType = UserContext;
    constructor(props) {
        super(props);
        this.state = {
            UserName: this.context,
        }

    }

    componentDidMount() {

    }
    componentDidUpdate() {

    }
    componentWillUnmount() {

    }

    render(){
        return (<div>
        <nav className="navbar navbar-expand-lg ">
            <div className="container-fluid">
                <Link className="navbar-brand" to={'/'}>
                    <img src="/images/logoZoo.png" alt="[logo]" className="logo-phyllium"/>
                    Le Zoo Phyllium
                </Link>
                <div className="collapse navbar-collapse" id="navbarContent">
                    <div className="navbar-nav me-auto mb-2 mb-lg-0">
                        <Link className="nav-link "  to={`/Animals`}>Les animaux</Link>
                        <UserContext.Consumer>
                            {value => value !== "" ?
                                <Link className="nav-link " to={`/FavAnimals`}>Mes Favoris</Link>
                                :
                                null
                            }
                        </UserContext.Consumer>
                        <AdminContext.Consumer>
                            {value => value !== "" ?
                                <Link className="nav-link" to={`/DisconnectUser`}>Gestion des utilisateurs</Link>
                                :
                                null
                            }
                        </AdminContext.Consumer>
                    </div>
                    <div className="d-flex">
                        <UserContext.Consumer>
                            {value => value !== "" ? null
                                :
                                <Link id="link-inscription" className="nav-link" to={`/Register`}>S'inscrire</Link>
                            }
                        </UserContext.Consumer>
                        <UserContext.Consumer>
                            {value => value !== "" ?
                                <Link className="nav-link" to={`/Disconnect`}>Se Déconnecter</Link>
                                :
                                <Link className="nav-link" to={`/Connect`}>Se connecter</Link>
                            }
                        </UserContext.Consumer>


                    </div>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </nav>
        <Outlet/>
    </div>)
    };
}
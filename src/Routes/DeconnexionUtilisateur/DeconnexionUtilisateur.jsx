import {
    Navigate,
} from "react-router-dom";
import React, {Component} from "react";
import AdminContext from "../../Context/AdminContext";

export default class DeconnexionUtilisateur extends Component{
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            users:[],
        }
    }

    loadUserEvent=()=> {
           fetch('/api/admin/connectedusers')
                .then((res) => res.json())
                .then((eventResponse) => {
                    //Mise à jour du composant, appel de render
                    this.setState({users: eventResponse})
                })

    }

    componentDidMount(){
        // Appel vers notre serveur
        this.loadUserEvent();
    }

    componentDidUpdate() {
        this.loadUserEvent();
    }

    async disconnectUser(id) {
        await fetch('/api/admin/disconnectuser/' + id)
        this.componentDidUpdate();
    }

     render(){
        return(
            <div className="container">
                <div className="row group-separation">
                    <h3>Liste des utilisateurs connectés</h3>
                    <AdminContext.Consumer>
                        {userName => userName === "" ?
                            <Navigate to="/*" />
                            :
                            <div>
                            {
                                this.state.users.length===0 ?
                                    <p>Aucun utilisateur n'est connecté</p>
                                    :
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Identifiant</th>
                                                <th>Nom d'utilisateur</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        { this.state.users.map(user=>{
                                            return(
                                                <tr>
                                                    <td>{user.id}</td>
                                                    <td>{user.login}</td>
                                                    <td>{user.email}</td>
                                                    <td>{user.roleId}</td>
                                                    <td>
                                                        <button className="btn btn-danger"
                                                            onClick={()=>this.disconnectUser(user.id)}>
                                                            Déconnecter
                                                        </button>
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                        </tbody>
                                    </table>
                            }
                            </div>
                        }
                    </AdminContext.Consumer>
                </div>
            </div>
        )
    }
}
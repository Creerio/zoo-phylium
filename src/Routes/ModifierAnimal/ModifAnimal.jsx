import {Form, redirect, useParams} from 'react-router-dom';
import React, { Component } from 'react';
import AnimalsMap from "../AnimalMap/AnimalMap";
import AdminContext from '../../Context/AdminContext';
import { Navigate } from 'react-router-dom';
function withParams(Component) {
  return props => <Component {...props} params={useParams()} />;
}

async function GetAnimal(id) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ animalId: id })
    };
    // Fetch the animal
    return await fetch('/api/admin/getanimaldata', requestOptions).then(async (res) => {
        return await res.json();
    });
}

export async function handleSubmit({ request, params }) {
    const formData = await request.formData();
    const animal = Object.fromEntries(formData);
    const res = await
        fetch('/api/admin/updateanimal',{
            method:'POST',
            body: JSON.stringify({
                animalId: params.id,
                animalName: animal.name,
                animalDescription: animal.description,
                animalDiet: animal.diet,
                animalHeight: animal.height,
                animalWeight: animal.weight,
                animalImgUrl: animal.imageUrl,
                animalPosX: animal.posX,
                animalPosY: animal.posY,
            }),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
    if (res.status===200){
        return(redirect("/Animals/"+ params.id));
    }
    else{
        document.getElementById("form-error").textContent = "La modification de l'animal a échoué. Veuillez réessayer."
    }
}

class modifAnimal extends Component{

    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'informations d'animaux vide
        this.state = {
            name: '',
            description: '',
            diet: '',
            height: '',
            weight: '',
            imageUrl: '',
            posX: '',
            posY: '',
        }
    }

    loadAnimalEvent=async () => {
        GetAnimal(this.props.params.id).then((animal) => {
            this.updateState(animal);
        });
    }

    updateState({ name, description, diet, height, weight, imageUrl, posX, posY }) {
        this.setState({
            file: {},
            name: name,
            description: description,
            diet: diet,
            height: height,
            weight: weight,
            imageUrl: imageUrl,
            posX: posX,
            posY: posY,
        })
    }

    componentDidMount() {
        this.loadAnimalEvent();
    }

    changePosX(e){
        if(e.currentTarget.value>95){
            this.setState({posX: 95})
        }
        else if(e.currentTarget.value<0){
            this.setState({posX: 0})
        }
        else{
            this.setState({posX: e.currentTarget.value})
        }
    }

    changePosY(e){
        if(e.currentTarget.value>95){
            this.setState({posY: 95})
        }
        else if(e.currentTarget.value<0){
            this.setState({posY: 0})
        }
        else{
            this.setState({posY: e.currentTarget.value})
        }
    }

    changeHeight(e){
        if(e.currentTarget.value<0){
            this.setState({height: -1})
        }
        else{
            this.setState({height: e.currentTarget.value})
        }
    }

    changeWeight(e){
        if(e.currentTarget.value<0){
            this.setState({weight: -1})
        }
        else{
            this.setState({weight: e.currentTarget.value})
        }
    }

    async askForFile(e) {
        // we'll get the first file in the file input
        const original = e.target.files[0];
        // no file received, exit
        if (!original)  return;

        const formData = new FormData();
        formData.append("image",original,original.name);

        console.log(original.name);
        await fetch('/api/admin/addanimalimage/'+original.name, {
            method: 'POST',
            body: formData
        })
        this.setState({imageUrl: "images/animals/" + original.name})

    }
    render(){
        return (
            <AdminContext.Consumer>
                {userName => userName === "" ?
                    <Navigate to="/*" />
                    :
                <div className="container text-center" id="connection">
                    <div className="row group-separation justify-content-center" id="sidebar">
                        <h3>Modifier un animal</h3>
                        <Form method="POST">
                            <div className="d-flex justify-content-center row group-separation">

                                <div className="form-group col-8">
                                    <label htmlFor="diet">Nom de l'animal:</label>
                                    <input id="name" name="name" className="form-control" type={"text"} value={this.state.name}
                                           onChange={(e)=>this.setState({name : e.currentTarget.value})} required/>
                                </div>
                            </div>
                            <div className="d-flex justify-content-center row group-separation">
                                <div className="form-group col-2">
                                    <label htmlFor="height">Taille moyenne (en m):</label>
                                    <input id="height" name="height" className="form-control" type={"number"} value={this.state.height}
                                           onChange={(e)=>this.changeHeight(e)}
                                           placeholder="-1 si inconnu" required/>
                                </div>
                                <div className="form-group col-2">
                                    <label htmlFor="weight">Poids moyen (en kg):</label>
                                    <input id="weight" name="weight" className="form-control" type={"number"} value={this.state.weight}
                                           onChange={(e)=>this.changeWeight(e)}
                                           placeholder="-1 si inconnu" required/>
                                </div>
                                <div className="form-group col-4">
                                    <label htmlFor="diet">Régime alimentaire:</label>
                                    <input id="diet" name="diet" className="form-control" type={"text"} value={this.state.diet}
                                           onChange={(e)=>this.setState({diet : e.currentTarget.value})} required/>
                                </div>
                            </div>
                            <div className="d-flex justify-content-center row group-separation">
                                <div className="form-group col-8">
                                    <label htmlFor="description">Description de l'animal:</label>
                                    <textarea id="description" name="description" className="form-control" value={this.state.description}
                                              onChange={(e)=>this.setState({description : e.currentTarget.value})} required/>
                                </div>
                            </div>
                            <br/><hr/>
                            <div className="d-flex justify-content-center row group-separation">
                                <div className="form-group col-4">
                                    <label htmlFor="imageUrl">Lien de l'image dans /public:</label>
                                    <form>
                                        <input id="imageUrl" name="imageUrl" className="form-control" type={"file"} accept="image/*" onChange={e=>this.askForFile(e)}/>
                                    </form>
                                    <input id="description" name="imageUrl" className="form-control" type={"text"} value={this.state.imageUrl}
                                           onChange={(e)=>this.setState({imageUrl : e.currentTarget.value})} required/>
                                </div>
                                <div className="form-group col-2">
                                    <img src={"/"+this.state.imageUrl} className="map-animal-thumbnail rounded-circle" alt=""/>
                                </div>
                                <small className="form-text text-muted">
                                    Exemple de lien fonctionnel : 'images/red.png'
                                </small>
                            </div>
                            <div className="d-flex justify-content-center row group-separation">
                                <div className="form-group col-2">
                                    <label htmlFor="posX">Position x sur la carte:</label>
                                    <input id="posX" name="posX" className="form-control" type={"number"} min ="0" max="95" value={this.state.posX}
                                           onChange={(e)=>this.changePosX(e)} required/>
                                </div>
                                <div className="form-group col-2">
                                    <label htmlFor="posY">Position y sur la carte:</label>
                                    <input id="posY" name="posY" className="form-control" type={"number"} min ="0" max="95" value={this.state.posY}
                                           onChange={(e)=>this.changePosY(e)} required/>
                                </div>
                                <small className="form-text text-muted">
                                    Voir-ci dessous (point rouge) pour placer correctement l'animal sur la carte.
                                </small>
                            </div>
                            <p id="form-error" className="form-error"></p>
                            <div className="d-flex justify-content-center row group-separation">
                                <div className="form-group col-2">
                                    <button name="animalId" type="submit" className="btn btn-primary">Modifier</button>
                                </div>
                            </div>
                        </Form>
                    </div>
                    <div className="map-wrapper">
                        <AnimalsMap/>
                        <div style={{left: this.state.posX + '%', top: this.state.posY + '%'}}>
                            <img className="svg-icon-animal-pos" src="/logo/arrow-drop-down-fill.svg" style={{left: this.state.posX + '%', top: parseInt(this.state.posY) + 4 + '%'}} alt=""/>
                            <img src="/images/red.png" className="map-animal-thumbnail rounded-circle" style={{left: this.state.posX + '%', top: this.state.posY + '%'}} alt=""/>
                        </div>
                    </div>
                </div>
                }
            </AdminContext.Consumer>
        );
    }
}


export default withParams(modifAnimal);


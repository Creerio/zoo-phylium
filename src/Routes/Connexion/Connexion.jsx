import {
    Link, Form
} from "react-router-dom";
import {Component} from "react";
//import { getContacts, createContact } from "../contacts";
import './Connexion.css'

export async function connect({ request, params }) {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    console.log(updates)
    const res = await
        fetch('/api/authentificate',{
            method:'POST',
            body: JSON.stringify({
                login : updates.login,
                passwd : updates.passwd
            }),
            headers: {"Content-type": "application/json; charset=UTF-8"}

        });
    if (res.status!==200){
        document.getElementById("form-error").textContent = "Le mot de passe est incorrect."
    }
    else{
        console.log("redirection")
        //je n'ai pas réussi à redessiner uniquement le composant menu
        return(location.assign("/"))
        //Accueil.render()
    }

}

export default class Connexion extends Component{
    //const { contacts } = useLoaderData();
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            login:'',
            pwd:'',
        }
    }

    render(){
        return (
            <div className="container text-center" id="connection">
                <div className="row group-separation justify-content-center" id="sidebar">
                    <h3>Se connecter</h3>
                    <Form method="POST">
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="login">Login:</label>
                                <input name="login" type={"text"} value={this.state.login} onChange ={(e)=>this.setState({login : e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="passwd">Mot de passe:</label>
                                <input name="passwd" type={"password"} value={this.state.pwd} onChange ={(e)=>this.setState({pwd : e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <p id="form-error" className="form-error"></p>
                        <button type="submit" className="btn btn-primary">Se connecter</button>
                    </Form>
                </div>
                <br/><hr/>
                <div className="group-separation">
                    <h4>Nouveau sur le site?</h4>
                    <Link to={`/Register`}>
                        <button className="btn btn-secondary">Créer un compte</button>
                    </Link>
                </div>
            </div>
        );
    }
}
import {
    Form, redirect,
} from "react-router-dom";
import {Component} from "react"
import './Enregistrer.css'

function validateEmail(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}

export async function register({ request, params }) {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    if (validateEmail(updates.email)) {
        if (updates.pwd && updates.pwd === updates.pwdconf){
            const res = await
                fetch('/api/register',{
                    method:'POST',
                    body: JSON.stringify({
                        login : updates.login,
                        passwd : updates.pwd,
                        email : updates.email
                    }),
                    headers: {"Content-type": "application/json; charset=UTF-8"}
                });
            if (res.status!==200){
                document.getElementById("form-error").textContent = "La création du compte a échoué. Veuillez réessayer."
            }
            else{
                console.log("redirection");
                //je n'ai pas réussi à redessiner uniquement le composant menu
                return(redirect("/Connect"));
                //Accueil.render()
            }
        } else {
            document.getElementById("form-error").textContent = "Les mots de passe saisis sont différents."
        }
    } else {
        document.getElementById("form-error").textContent = "L'adresse mail saisie est incorrecte."
    }
}

export default class Enregistrer extends Component{
    //const { contacts } = useLoaderData();
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            login:'',
            pwd:"",
            pwdconf:"",
            email:'',
            pwdcorrect:false
        }
    }

    render(){
        return (
            <div className="container text-center" id="inscription">
                <div className="row group-separation justify-content-center">
                    <h3>Inscription</h3>
                    <Form method="post">
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="login">Adresse mail:</label>
                                <input name="email" type={"text"} value={this.state.email}
                                       onChange={(e) => this.setState({email: e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="login">Login:</label>
                                <input name="login" type={"text"} value={this.state.login}
                                       onChange={(e) => this.setState({login: e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="pwd">Mot de passe:</label>
                                <input name="pwd" type={"password"} value={this.state.pwd}
                                       onChange={(e) => this.setState({pwd: e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center row group-separation">
                            <div className="form-group col-4">
                                <label htmlFor="pwdconf">Confirmation du mot de passe</label>
                                <input name="pwdconf" type={"password"} value={this.state.pwdconf}
                                       onChange={(e) => this.setState({pwdconf: e.currentTarget.value})}
                                       className="form-control" required/>
                            </div>
                        </div>
                        <p id="form-error" className="form-error"></p>
                        <button type="submit" className="btn btn-primary">Créer un compte</button>
                    </Form>
                </div>
            </div>

        );
    }
}
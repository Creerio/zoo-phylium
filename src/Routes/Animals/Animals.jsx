import React from "react";
import {Form, Link} from "react-router-dom";
import Menu from "../Menu/Menu";
import UserContext from "../../Context/UserContext";
import AdminContext from '../../Context/AdminContext';
import './Animals.css'

export default class Animals extends React.Component {
    static contextType = Menu.contextType;
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'animaux vide
        this.state = {
            animals: [],
            id : [],
            searchName : ""
        }
    }

    async addDelFav(id) {
        await fetch('/api/adddelfav', {
            method: 'POST',
            body: JSON.stringify({
                animalId: id,
            }),
            headers: { "Content-type": "application/json; charset=UTF-8" }

        });
    }

    loadAnimalsEvent=()=> {
        if (this.state.searchName.length !== 0) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ animalQuery: this.state.searchName })
            };
            fetch('/api/searchanimal', requestOptions)
                .then((res) => res.json())
                .then((eventResponse) => {
                    //Mise à jour du composant, appel de render
                    console.log(eventResponse)
                    console.log(this.state.animals)
                    this.setState({animals: eventResponse})
                })
        } else {
            fetch('/api/animals')
                .then((res) => res.json())
                .then((eventResponse) => {
                    // on met à jour l'état de notre composant
                    // ce qui forcera son rendu, donc l'appel à la méthode render
                    this.setState({animals: eventResponse})
                })
        }
    }

    componentDidMount() {
        // Appel vers notre serveur
        this.loadAnimalsEvent();
    }

    async changeFavStar(id) {
        await this.addDelFav(id);
        //changement de t.src en fonction de l'image actuelle ou de l'état actuel du favori de l'animal
        this.loadAnimalsEvent();
     }

    async deleteAnimal(id) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ animalId: id })
        };
        await fetch('/api/admin/delanimal', requestOptions);
        this.loadAnimalsEvent();
        return null;
    }

    render(){
        return(<div className="container">
            <div className="group-separation d-flex justify-content-center">
                <div className="align-items-stretch">
                    <div className="card">
                        <div className="card-header">
                            Rechercher un animal
                        </div>
                        <AdminContext.Consumer>
                            {userName => userName !== "" ?
                                <div className="card-body">
                                    <div className="d-flex align-items-stretch" >
                                        <input type="text" className="form-control" id="searchName" name="searchName" placeholder="Nom de l'animal" onChange ={(e)=>this.setState({searchName: e.currentTarget.value})} value={this.state.searchName}/>
                                        <button name="animalId" type="submit" className="btn btn-primary btn-fright" onClick={e=>this.loadAnimalsEvent()}>Rechercher</button>
                                    </div>
                                    <hr/>
                                    <div className="d-flex justify-content-center">
                                        <Link to={`/AddAnimal`}>
                                            <button className="btn btn-primary">+ Ajouter un animal</button>
                                        </Link>
                                    </div>
                                </div>
                                :
                                <div className="card-body">
                                    <div className="d-flex align-items-stretch" >
                                        <input type="text" className="form-control" id="searchName" name="searchName" placeholder="Nom de l'animal" onChange ={(e)=>this.setState({searchName: e.currentTarget.value})} value={this.state.searchName}/>
                                        <button name="animalId" type="submit" className="btn btn-primary btn-fright" onClick={e=>this.loadAnimalsEvent()}>Rechercher</button>
                                    </div>
                                </div>
                            }
                        </AdminContext.Consumer>
                    </div>
                </div>
            </div>
            <div className="row group-separation">
                {
                    this.state.animals.map(animal=>{
                        return(
                            <div id={`animal-card-${animal.id}`} className="col-lg-3 col-md-4 col-sm-6 d-flex align-items-stretch group-separation">
                                <div className="card">
                                    <AdminContext.Consumer>
                                        {userName => userName !== "" ?
                                            <div className="card-header">
                                                {animal.name}
                                                <Form method="POST" >
                                                    <button name="animalId" type="submit" value={animal.id} onClick={()=>this.deleteAnimal(animal.id)} className="btn btn-danger btn-fright">
                                                        <img className="svg-icon-white" src="/logo/delete-bin-2-line.svg" alt="delete"/>
                                                    </button>
                                                </Form>
                                                <Link to={`/ModifyAnimal/${animal.id}`}>
                                                    <button name="animalId" className="btn btn-primary btn-fright">
                                                        <img className="svg-icon-white" src="/logo/edit-line.svg" alt="edit"/>
                                                    </button>
                                                </Link>
                                            </div>
                                            :
                                            <div className="card-header">
                                                {animal.name}
                                            </div>
                                        }
                                    </AdminContext.Consumer>
                                    <div className="card-img-top">
                                        <img src={"/"+animal.imageUrl} className="img-fluid card-animal-thumbnail" alt="animalThumbnail"></img>
                                    </div>
                                    <div className="card-footer card-animal-footer">
                                        <Link to={`/Animals/${animal.id}`}>
                                            <button type="button" className="btn btn-primary btn-fright">Voir +</button>
                                        </Link>
                                        <UserContext.Consumer>
                                            {userName => userName !== "" ?
                                                animal.fav ?
                                                    <button id={animal.id} name="animalId" type={"submit"} value={animal.id} onClick ={()=>this.changeFavStar(animal.id)} className="btn btn-primary btn-fright">
                                                        <img id={"fav-" + animal.id} className="svg-icon-white" src="/logo/star-fill.svg" alt="isFav"/>
                                                    </button>
                                                    :
                                                    <button name="animalId" type="submit" value={animal.id} onClick={()=>this.changeFavStar(animal.id)} className="btn btn-primary btn-fright">
                                                        <img id={"fav-" + animal.id} className="svg-icon-white" src="/logo/star-line.svg" alt="isNotFav"/>
                                                    </button>
                                                :
                                                null
                                            }
                                        </UserContext.Consumer>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>)
    }
}
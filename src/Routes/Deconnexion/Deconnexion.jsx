import {Component} from "react";
export async function disconnect() {
    await fetch('/api/selfdisconnect')
    return(location.assign("/"))
}

export default class Deconnexion extends Component{
    //const { contacts } = useLoaderData();
    constructor(props) {
        super(props);
        // initialisation de l'état du composant
        // ici un tableau d'événements vide
        this.state = {
            login:'',
            pwd:'',
        }
    }

    async componentDidMount() {
        await disconnect();
    }

    render(){
        return null
    }
}
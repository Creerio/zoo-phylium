import React from "react";
import {Link} from "react-router-dom";
import "./NoPage.css"

/**
 * 404 page
 */
export default class NoPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.render();
    }

    render(){
        return(
            <div id="unknown-page">
                <div className="banner-wrapper">
                    <img src="/images/banner.jpg" className="banner-img"/>
                    <div className="banner-txt">
                        <h1>404</h1>
                        <h2>La page que vous recherchez est inaccessible.</h2>
                        <Link to={``}>
                            <button type="button" className="btn btn-primary">Retour à l'accueil</button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }


}

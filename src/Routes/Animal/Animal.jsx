import React from "react";
import { Await, useParams, useAsyncValue, Form, Link } from 'react-router-dom';
import UserContext from "../../Context/UserContext";

/**
 * Gets the animal from our server
 *
 * @param id
 * Animal ID
 *
 * @returns {Promise<any>}
 */
async function getAnimal(id) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ animalId: id })
    };
    // Fetch the animal
    const animal = await fetch('/api/animals', requestOptions).then(async (res) => {
        return await res.json();
    });
    return animal;
}
async function changeFavStar(id) {
    let t = document.getElementById('fav-' + id);
    //changement de t.src en fonction de l'image actuelle ou de l'état actuel du favori de l'animal
    t.src.includes('star-line') ? t.src = t.src.replace('star-line', 'star-fill') : t.src = t.src.replace('star-fill', 'star-line');
    await fetch('/api/adddelfav', {
        method: 'POST',
        body: JSON.stringify({
            animalId: id,
        }),
        headers: { "Content-type": "application/json; charset=UTF-8" }

    });
}

/**
 * Renders the required animal
 *
 * @returns {JSX.Element}
 *
 * @constructor
 */
function RenderAwaitedAnimal() {
    let dt = useAsyncValue();

    return (
        <div className="container">
            <div className="group-separation">
                <div className="card">
                    <h3 className="card-header">
                        <strong>{dt.name}</strong>
                    </h3>
                    <div className="row">
                        <div className="col-lg-5 col-sm-12">
                            <img src={"/"+dt.imageUrl} className="img-fluid card-animal-thumbnail"/>
                        </div>
                        <div className="card-body col-lg-7 col-sm-12 text-center">
                            <h4>Quelques informations sur moi :</h4>
                            <ul>
                                <li>Je suis <strong>{dt.diet}</strong>.</li>
                                {dt.height != -1 ?
                                    <li>Je fais environ <strong>{dt.height}</strong> mètres.</li>
                                    : null
                                }
                                {dt.weight != -1 ?
                                    <li>Je pèse environ <strong>{dt.weight}</strong> kilos.</li>
                                    : null
                                }
                            </ul>
                            <p>
                                J'ai hâte de vite vous retrouver au zoo Phyllium,
                                que ce soit dans un enclos ou dans votre assiette! ;)
                            </p>
                        </div>
                        <div className="card-body col-12 text-center">
                            <h4>D'autres informations?</h4>
                            <p>{dt.description}</p>
                        </div>
                    </div>
                    <UserContext.Consumer>
                        {userName => userName !== "" ?
                            dt.fav ?
                                <div className="card-footer">
                                    <Link to={`/Animals`}>
                                        <button type="button" className="btn btn-primary btn-fleft">Retour à la liste des animaux</button>
                                    </Link>
                                    <button name="animalId" className="btn btn-primary btn-fright" type={"submit"} value={dt.id} onClick ={(e)=>changeFavStar(dt.id)}>
                                        <img id={"fav-" + dt.id} className="svg-icon-white" src="/logo/star-fill.svg" />
                                    </button>
                                </div>
                                :
                                <div className="card-footer">
                                    <Link to={`/Animals`}>
                                        <button type="button" className="btn btn-primary btn-fleft">&lt;- Retour</button>
                                    </Link>
                                    <button name="animalId" type="submit" className="btn btn-primary btn-fright" value={dt.id} onClick={(e)=>changeFavStar(dt.id)}>
                                        <img id={"fav-" + dt.id} className="svg-icon-white" src="/logo/star-line.svg" />
                                    </button>
                                </div>
                            :
                            <div className="card-footer">
                                <Link to={`/Animals`}>
                                    <button type="button" className="btn btn-primary btn-fleft">&lt;- Retour</button>
                                </Link>
                            </div>
                        }
                    </UserContext.Consumer>
                </div>
            </div>
        </div>
    )
}

export default () => {
    // Get the id of the page
    const { id } = useParams();

    // Get the animal, at least the promised animal
    const animal = getAnimal(id);
    return (
        <div>
            <React.Suspense
                fallback={<div>Veuillez patienter, votre animal arrive :)</div>}
            >
                <Await
                    resolve={animal}
                    errorElement={
                        <div>Erreur, on ne peut pas récupérer votre animal :/</div>
                    }
                    >
                    <RenderAwaitedAnimal/>
                </Await>
            </React.Suspense>
        </div>
    )
}
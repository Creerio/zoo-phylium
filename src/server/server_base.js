// Express server for website
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');

// Server port, defined by either the user, or defaults to 8080
process.env.PORT = process.env.PORT ? process.env.PORT : 8080;


// JSON is required for data usage
app.use(express.json());

// Cookies for the tokens
app.use(cookieParser());

// Allow access to public folders
app.use(express.static('public'));

module.exports = { express, app };
// Required models for API
const { databaseInstance } = require('../../class/Database');
const Animals = databaseInstance.db.modelManager.getModel('Animals');
const Favorites = databaseInstance.db.modelManager.getModel('Favorites');

// Our app
const { app } = require('./server_base');

// Required handler for new users or connecting ones
const { userHandler } = require('../../class/UserHandler');


async function fetchAllAnimals(req) {
    const animals = await Animals.findAll();
    // Only allow for necessary information, nothing else
    for (let i = 0; i < animals.length; i++) {
        animals[i] = animals[i].infos;
    }

    // If user is connected, we have to get if the user added them as a favorite
    if (req.cookies.token) {
        const user = await userHandler.getUser(req.cookies.token);

        // Get favorites id's
        const favIds = await Favorites.findAll({
            where: {
                userId: user.id,
            },
        });
        for (let i = 0; i < favIds.length; i++) {
            favIds[i] = favIds[i].animalId;
        }

        for (const animal of animals) {
            animal.fav = favIds.includes(animal.id);
        }

    }
    return animals;
}


/**
 * Gives out every single animal's information, except their location
 */
app.get('/api/animals', async (req, res) => {
    try {
        const animals = await fetchAllAnimals(req);

        res.send(animals);
    }
    catch (e) {
        res.send(null);
    }
});

/**
 * Gives out an animal's information, except their location
 */
app.post('/api/animals', async (req, res) => {
    const animalId = req.body.animalId;
    try {
        const animal = (await Animals.findOne({
            where: {
                id: animalId,
            },
        })).infos;

        // If user is connected, we have to get if the user added it as a favorite
        if (req.cookies.token) {
            const user = await userHandler.getUser(req.cookies.token);

            const fav = await Favorites.findOne({
                where: {
                    userId: user.id,
                    animalId: animalId,
                },
            });

            animal.fav = (fav != null);
        }
        res.send(animal);
    }
    catch (e) {
        res.send(null);
    }
});

/**
 * Gives out every single animal's location
 */
app.get('/api/pos', async (req, res) => {
    try {
        const animals = await Animals.findAll();
        // Only allow for necessary information, nothing else
        for (let i = 0; i < animals.length; i++) {
            animals[i] = animals[i].pos;
        }
        res.send(animals);
    }
    catch (e) {
        res.send(null);
    }
});

/**
 * Gives out an animal's location
 */
app.post('/api/pos', async (req, res) => {
    const animalId = req.body.animalId;
    try {
        const animal = await Animals.findOne({
            where: {
                id: animalId,
            },
        });
        res.send(animal.pos);
    }
    catch (e) {
        res.send(null);
    }
});


/**
 * Gives out every animal starting with given answer
 */
app.post('/api/searchanimal', async (req, res) => {
    // Either give, or get nothing
    if (!req.body.animalQuery || req.body.animalQuery === '') {
        res.send([]);
        return;
    }

    // Store the given information
    const startsWith = req.body.animalQuery;

    try {
        let animals = await fetchAllAnimals(req);

        // Filter all the animals to be identical, while sequelize does bundle something for us, this is better since we are taking some characters into account that sequelize does not
        animals = animals.filter(function(elem) {
            // Store as a lowercase character with no special character
            const name = elem.name.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();

            // If it doesn't start with what we have given, it will be removed
            return name.startsWith(startsWith.toLowerCase());
        });

        animals.length === 0 ? res.send([]) : res.send(animals);

    }
    catch (e) {
        res.send(null);
    }
});
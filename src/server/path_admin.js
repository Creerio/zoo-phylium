// Required model for API
const { databaseInstance } = require('../../class/Database');
const Animals = databaseInstance.db.modelManager.getModel('Animals');
const Favorites = databaseInstance.db.modelManager.getModel('Favorites');
const Role = databaseInstance.db.modelManager.getModel('Role');
const Users = databaseInstance.db.modelManager.getModel('User');
const Token = databaseInstance.db.modelManager.getModel('Token');

// Our app
const { app } = require('./server_base');

// Required handler for new users or connecting ones
const { userHandler } = require('../../class/UserHandler');

// Handle animal images
const multer = require('multer');
const upload = multer({ dest: './public/images/animals/' });

// Role needed for these pages
const roleName = 'Admin';

/**
 * Checks if a given animal has the required elements for the database
 *
 * @param requestBody
 * Body containing our animal information
 *
 * @returns {boolean}
 */
function animalInfoMissing(requestBody) {
    return (!(requestBody.animalName) || !(requestBody.animalDescription) || !(requestBody.animalDiet) || !(requestBody.animalHeight) || !(requestBody.animalWeight) || !(requestBody.animalImgUrl) || !(requestBody.animalPosX) || !(requestBody.animalPosY));
}

/**
 * Checks if the user going to the admin pages is an admin
 */
app.all('/api/admin/*', async (req, res, next) => {
    if (req.cookies.token) {
        // User MUST be an admin
        const user = await userHandler.getUser(req.cookies.token);
        const isAdmin = await userHandler.checkRole(user, roleName);

        isAdmin ? next() : res.sendStatus(403);
    }
    else {
        res.sendStatus(403);
    }
});

/**
 * Add a new animal
 */
app.post('/api/admin/addanimal', async (req, res) => {
    if (req.cookies.token) {
        // Required elements : animalName, animalDescription, animalDiet, animalHeight, animalWeight, animalImgUrl, animalPosX, animalPosY
        // No image "stream" will be provided for the animal, yet.
        if (animalInfoMissing(req.body)) {
            res.status(403).send('There is something missing. Required : animalName, animalDescription, animalDiet, animalHeight, animalWeight, animalImgUrl, animalPosX, animalPosY');
        }
        else {
            // Can fail, since some values may cause a database issue
            try {
                const createdAnimal = await Animals.create({
                    name: req.body.animalName,
                    description: req.body.animalDescription,
                    diet: req.body.animalDiet,
                    height: req.body.animalHeight,
                    weight: req.body.animalWeight,
                    imageUrl: req.body.animalImgUrl,
                    posX: req.body.animalPosX,
                    posY: req.body.animalPosY,
                });
                res.status(200).send({
                    id : createdAnimal.id,
                });
            }
            catch (e) {
                res.status(403).send('Failed to insert the new animal');
                console.error(e);
            }
        }
    }
});

/**
 * Add a new image for an animal
 */
app.post('/api/admin/addanimalimage/:fileName', upload.single('image'), (req, res) => {
    // Image has been uploaded, we have to rename it now if a name is given
    if (req.params.fileName) {
        require('fs').renameSync(req.file.path, req.file.destination + req.params.fileName);
        res.sendStatus(200);
    }
    // Or nothing is given, we have to give back the file name
    else {
        res.status(200).send(req.file.filename);
    }


});

/**
 * Update an animal
 */
app.post('/api/admin/updateanimal', async (req, res) => {
    if (req.cookies.token) {
        if (!req.body.animalId) {
            res.status(403).send('There is something missing. Required : animalId');
        }
        else {
            // Check first if our animal exists, to modify it later
            const toUpdate = await Animals.findOne({
                where: {
                    id: req.body.animalId,
                },
            });

            if (!toUpdate) {
                res.sendStatus(403);
                return;
            }

            // It does, we need other elements to modify it
            // Required elements : animalName, animalDescription, animalDiet, animalHeight, animalWeight, animalImgUrl, animalPosX, animalPosY
            if (animalInfoMissing(req.body)) {
                res.status(403).send('There is something missing. Required : animalName, animalDescription, animalDiet, animalHeight, animalWeight, animalImgUrl, animalPosX, animalPosY');
            }
            else {
                // Can fail, since some values may cause a database issue
                toUpdate.update({
                    name: req.body.animalName,
                    description: req.body.animalDescription,
                    diet: req.body.animalDiet,
                    height: req.body.animalHeight,
                    weight: req.body.animalWeight,
                    imageUrl: req.body.animalImgUrl,
                    posX: req.body.animalPosX,
                    posY: req.body.animalPosY,
                })
                    .then(() => {
                        res.sendStatus(200);
                    })
                    .catch((e) => {
                        res.status(403).send('Failed to update the animal');
                        console.error(e);
                    });
            }
        }
    }
});

/**
 * Delete an animal
 */
app.post('/api/admin/delanimal', async (req, res) => {
    if (req.cookies.token) {
        // Required elements : animalId
        if (!(req.body.animalId)) {
            res.status(403).send('There is something missing. Required : animalId');
        }
        else {
            // Find our animal to delete it
            const animal = await Animals.findOne({
                where: {
                    id: req.body.animalId,
                },
            });

            if (animal) {
                // Remove the animal from the favorite elements
                const favIds = await Favorites.findAll({
                    where: {
                        animalId: animal.id,
                    },
                });

                // Removal
                for (const element of favIds) {
                    await element.destroy();
                }
                await animal.destroy();
            }
            res.sendStatus(200);
        }
    }
    else {
        res.sendStatus(403);
    }
});


/**
 * Disconnects a given user
 */
app.get('/api/admin/disconnectuser/:userId', async (req, res) => {
    if (req.cookies.token) {
        if (!req.params.userId) {
            res.status(403).send('Missing user id');
        }
        else {
            // Check if the given user is an admin
            const user = await Users.findOne({
                where: {
                    id: req.params.userId,
                },
            });
            const isAdmin = await userHandler.checkRole(user, roleName);
            if (isAdmin) {
                res.sendStatus(403);
            }
            else {
                await userHandler.disconnectUser(req.params.userId);
                res.sendStatus(200);
            }

        }
    }
    else {
        res.sendStatus(403);
    }
});


/**
 * Gives out an animal's full information, this is useful when modifying one
 */
app.post('/api/admin/getanimaldata', async (req, res) => {
    // The admin MUST still be connected
    if (req.cookies.token) {
        const animalId = req.body.animalId;
        try {
            const animal = (await Animals.findOne({
                where: {
                    id: animalId,
                },
            }));

            res.send(animal);
        }
        catch (e) {
            res.send(null);
        }
    }
    else {
        res.send(null);
    }
});

/**
 * Gives the currently connected users
 */
app.get('/api/admin/connectedusers', async (req, res) => {
    if (req.cookies.token) {
        try {
            const connectedUsersIds = await Token.findAll();

            // Only the user's id is needed
            for (let i = 0; i < connectedUsersIds.length; i++) {
                connectedUsersIds[i] = connectedUsersIds[i].userId;
            }

            // Fetch the users data
            let connectedUsers = await Users.findAll({
                where: {
                    id: connectedUsersIds,
                },
            });

            // Admin's roleId is required for an admin to be removed from the given list
            const adminId = await Role.findOne({
                where: {
                    name: roleName,
                },
            }).then((roleDT) => {
                return roleDT.id;
            });
            // No admin
            connectedUsers = connectedUsers.filter(function(user) {
                return user.roleId !== adminId;
            });

            // Keep only the bare minimum
            for (let i = 0; i < connectedUsers.length; i++) {
                connectedUsers[i] = connectedUsers[i].infos;
            }

            res.send(connectedUsers);
        }
        catch (e) {
            res.send(null);
        }
    }
    else {
        res.send(null);
    }
});

/**
 * Gives an answer based on if the user is an admin or not
 */
app.get('/api/isadmin', async (req, res) => {
    if (req.cookies.token) {
        const user = await userHandler.getUser(req.cookies.token);
        const isAdmin = await userHandler.checkRole(user, roleName);

        isAdmin ? res.send(user.login) : res.send('');
    }
    else {
        res.send('');
    }
});
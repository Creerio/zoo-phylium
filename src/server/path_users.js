// Required models for API
const { databaseInstance } = require('../../class/Database');
const Animals = databaseInstance.db.modelManager.getModel('Animals');
const Favorites = databaseInstance.db.modelManager.getModel('Favorites');
const Role = databaseInstance.db.modelManager.getModel('Role');
const Users = databaseInstance.db.modelManager.getModel('User');

// Our app
const { app } = require('./server_base');

// Required handler for new users or connecting ones
const { userHandler } = require('../../class/UserHandler');

/**
 * Always check for the user's token, they may give an invalid one
 */
app.all('*', async (req, res, next) => {
    if (req.cookies.token) {
        // Check if the token is valid
        const isValidToken = await userHandler.checkToken(req.cookies.token);
        if (!isValidToken) {
            delete req.cookies.token;
            res.clearCookie('token');
        }
    }

    next();
});

/**
 * Register as a new user
 */
app.post('/api/register', async (req, res) => {
    if (req.cookies.token) {
        res.sendStatus(403);
        return;
    }

    // Required elements : login, passwd, email
    if (!(req.body.login && req.body.passwd && req.body.email)) {
        res.status(403).send('There is something missing. Required : login, password and email');
    }
    else {
        // Get user role
        const role = await Role.findOne({
            where: {
                name: 'User',
            },
        });

        // Will either add the user, or not if is already exists
        await userHandler.addUser({
            login: req.body.login,
            passwd: req.body.passwd,
            email: req.body.email,
            roleId: role.id,
        });

        res.sendStatus(200);
    }
});

/**
 * User Authentification
 */
app.post('/api/authentificate', async (req, res) => {
    if (req.body.login && req.body.passwd) {
        const login = req.body.login;
        try {
            let user = await Users.findOne({
                where: {
                    login: login,
                },
            });
            // Wrong login
            if (!user) {
                res.status(403).send('Invalid login/password');
            }
            // Success
            else {
                const passwd = req.body.passwd;

                // Incorrect password
                const validPasswd = await userHandler.checkPasswd(user, passwd);
                if (!validPasswd) {
                    res.status(403).send('Invalid login/password');
                    return;
                }


                user = await Users.findOne({
                    where: {
                        login: login,
                    },
                });
                // Token expires at the same time as the cookie (3h)
                res.cookie('token', await userHandler.addToken(user), { expires: new Date(Date.now() + 1000 * 60 * 60 * 3), httpOnly: true });
                res.sendStatus(200);
            }
        }
        catch (e) {
            console.error(e);
            res.status(403).send('There was an error while trying to authenticate you. Please try again later');
        }
    }
    else {
        res.sendStatus(403);
    }
});

/**
 * Updates the current user with new data
 */
app.post('/api/updateSelf', async (req, res) => {
    if (req.cookies.token) {

        // Required elements : login, oldPasswd, newPasswd, email
        if (!(req.body.login && req.body.oldPasswd && req.body.newPasswd && req.body.email)) {
            res.status(403).send('There is something missing. Required : login, old password, new password and email');
        }
        else {
            const user = await userHandler.getUser(req.cookies.token);

            const validPasswd = await userHandler.checkPasswd(user, req.body.oldPasswd);
            if (!validPasswd) {
                res.status(403).send('Invalid old password !');
            }
            else {
                await userHandler.updateUser(user, {
                    login: req.body.login,
                    passwd: req.body.newPasswd,
                    email: req.body.email,
                });
            }
        }
    }
    else {
        res.sendStatus(403);
    }
});

/**
 * Gives the current's user login
 */
app.get('/api/getselflogin', async (req, res) => {
    if (req.cookies.token) {
        const user = await userHandler.getUser(req.cookies.token);

        res.send(user.login);
    }
    else {
        res.send('');
    }
});

/**
 * Disconnects the current user
 */
app.get('/api/selfdisconnect', async (req, res) => {
    if (req.cookies.token) {
        const user = await userHandler.getUser(req.cookies.token);

        delete req.cookies.token;
        res.clearCookie('token');

        await userHandler.disconnectUser(user.id);
    }

    res.sendStatus(200);
});


/**
 * Adds an animal as a favorite
 */
app.post('/api/adddelfav', async (req, res) => {
    if (req.cookies.token && req.body.animalId) {
        const user = await userHandler.getUser(req.cookies.token);


        const findAnimal = await Animals.findOne({
            where: {
                id: req.body.animalId,
            },
        });
        // The animal must exist
        if (findAnimal == null) {
            res.sendStatus(403);
            return;
        }

        // Create the favorite, should it not exist before
        const [favorite, hasBeenCreated] = await Favorites.findOrCreate({
            where: {
                userId: user.id,
                animalId: req.body.animalId,
            },
        });

        // Existed before
        if (!hasBeenCreated) {
            await favorite.destroy();
            res.sendStatus(200);
        }
        // New favorite
        else {
            res.sendStatus(200);
        }
    }
    else {
        res.sendStatus(403);
    }
});

/**
 * Gives the favorites animals information of the current user
 */
app.get('/api/getfavs', async (req, res) => {
    if (req.cookies.token) {
        const user = await userHandler.getUser(req.cookies.token);

        // Get favorites id's
        const favIds = await Favorites.findAll({
            where: {
                userId: user.id,
            },
        });
        for (let i = 0; i < favIds.length; i++) {
            favIds[i] = favIds[i].animalId;
        }
        // Fetch animals information, nothing more
        const animals = await Animals.findAll({
            where: {
                id: favIds, // Id in our animal ids
            },
        });
        for (let i = 0; i < animals.length; i++) {
            animals[i] = animals[i].infos;
        }
        res.send(animals);

    }
    else {
        res.send([]);
    }
});

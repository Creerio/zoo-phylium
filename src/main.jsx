import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Animals from "./Routes/Animals/Animals"
import Animal from "./Routes/Animal/Animal"
import FavAnimals from './Routes/FavoriteAnimals/FavoriteAnimals';
import NoPage from "./Routes/NoPage/NoPage"
import Accueil from "./Routes/Accueil/Accueil"
import Menu from './Routes/Menu/Menu'
import Register, {register} from './Routes/Enregistrer/Enregistrer'
import Disconnect, {disconnect} from './Routes/Deconnexion/Deconnexion'
import Connect, {connect} from './Routes/Connexion/Connexion'
import AjoutAnimal, {addAnimal} from "./Routes/AjoutAnimal/AjoutAnimal";
import "./global.css"
import ModifAnimal, {handleSubmit} from './Routes/ModifierAnimal/ModifAnimal';
import DeconnexionUtilisateur from "./Routes/DeconnexionUtilisateur/DeconnexionUtilisateur";

const router = createBrowserRouter([
    {
        path:"/",
        element : <Menu />,
        children:[
            {
                path: "/",
                element: <Accueil />,
            },
            {
                path: "/Animals",
                element: <Animals />,
            },
            {
                path: "/Animals/:id",
                element: <Animal />,

            },
            {
                path: "/FavAnimals",
                element: <FavAnimals />,
            },
            {
                path: "/Connect",
                element: <Connect />,
                action: connect,
            },
            {
                path: "/Disconnect",
                element: <Disconnect />,
                action: disconnect,
            },
            {
                path: "/Register",
                element: <Register />,
                action: register,
            },
            {
                path: "/AddAnimal",
                element: <AjoutAnimal />,
                action: addAnimal,
            },
            {
                path:"/ModifyAnimal/:id",
                element: <ModifAnimal />,
                action : handleSubmit,
            },
            {
                path:"/DisconnectUser",
                element: <DeconnexionUtilisateur />,
                //action : handleSubmit
            },
            {
                path: '*',
                element: <NoPage />
            },
],}
]);

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);
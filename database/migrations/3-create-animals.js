'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Animals', {
            id: {
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                comment: 'Animal unique identifier',
                type: Sequelize.INTEGER,
            },
            name: {
                comment: 'Animal\'s name',
                type: Sequelize.STRING,
                unique: true,
            },
            description: {
                comment: 'Animal\'s description',
                type: Sequelize.TEXT,
            },
            diet: {
                comment: 'Animal\'s diet',
                type: Sequelize.STRING,
            },
            height: {
                comment: 'Animal\'s height',
                type: Sequelize.FLOAT,
            },
            weight: {
                comment: 'Animal\'s weight',
                type: Sequelize.FLOAT,
            },
            imageUrl: {
                comment: 'Animal\'s image URL',
                type: Sequelize.TEXT,
            },
            posX: {
                comment: 'Animal\'s X position on the zoo plan',
                type: Sequelize.FLOAT,
            },
            posY: {
                comment: 'Animal\'s Y position on the zoo plan',
                type: Sequelize.FLOAT,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Animals');
    },
};
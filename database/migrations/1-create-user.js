'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Users', {
            id: {
                primaryKey: true,
                autoIncrement: true,
                comment: 'User unique identifier',
                type: Sequelize.INTEGER,
            },
            login: {
                unique: true,
                comment: 'User\'s login',
                type: Sequelize.STRING,
            },
            email: {
                unique: true,
                comment: 'User\'s email',
                type: Sequelize.STRING,
            },
            passwd: {
                unique: true,
                comment: 'User\'s password',
                type: Sequelize.TEXT,
            },
            roleId: {
                allowNull: false,
                type: Sequelize.INTEGER,
                comment: 'Role\'s id',
                references: {
                    model: 'Roles',
                    key: 'id',
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Users');
    },
};

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        const currentTime = new Date(Date.now());
        return queryInterface.bulkInsert('Roles', [
            {
                id: 1,
                name: 'User',
                createdAt: currentTime,
                updatedAt: currentTime,
            },
            {
                id: 2,
                name: 'Admin',
                createdAt: currentTime,
                updatedAt: currentTime,
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Roles', null, {});
    },
};

'use strict';

const bcrypt = require('bcrypt');
const salt = process.env.SALT_ROUNDS ? process.env.SALT_ROUNDS : 10;

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        const currentTime = new Date(Date.now());
        return queryInterface.bulkInsert('Users', [
            {
                login: 'user',
                passwd: await bcrypt.hash('user', salt),
                email: 'user@test.org',
                roleId: 1,
                createdAt: currentTime,
                updatedAt: currentTime,
            },
            {
                login: 'admin',
                passwd: await bcrypt.hash('admin', salt),
                email: 'admin@test.org',
                roleId: 2,
                createdAt: currentTime,
                updatedAt: currentTime,
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Users', null, {});
    },
};

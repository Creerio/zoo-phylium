'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Favorites extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Favorites.init({
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        userId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            comment: 'User\'s id',
        },
        animalId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            comment: 'Animal\'s id',
        },
    }, {
        sequelize,
        modelName: 'Favorites',
    });
    return Favorites;
};
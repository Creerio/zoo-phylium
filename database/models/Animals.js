'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Animals extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsToMany(models.User, { through: 'Favorites' });
        }
    }
    Animals.init({
        id: {
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            comment: 'Animal unique identifier',
            type: DataTypes.INTEGER,
        },
        name: {
            comment: 'Animal\'s name',
            type: DataTypes.STRING,
            unique: true,
        },
        description: {
            comment: 'Animal\'s description',
            type: DataTypes.TEXT,
        },
        diet: {
            comment: 'Animal\'s diet',
            type: DataTypes.STRING,
        },
        height: {
            comment: 'Animal\'s height',
            type: DataTypes.FLOAT,
        },
        weight: {
            comment: 'Animal\'s weight',
            type: DataTypes.FLOAT,
        },
        imageUrl: {
            comment: 'Animal\'s image URL',
            type: DataTypes.TEXT,
        },
        posX: {
            comment: 'Animal\'s X position on the zoo plan',
            type: DataTypes.FLOAT,
        },
        posY: {
            comment: 'Animal\'s Y position on the zoo plan',
            type: DataTypes.FLOAT,
        },
        infos: {
            comment: 'Only necessary information\'s about a given animal',
            type: DataTypes.VIRTUAL,
            get() {
                return {
                    id: this.id,
                    name: this.name,
                    description: this.description,
                    diet: this.diet,
                    height: this.height,
                    weight: this.weight,
                    imageUrl: this.imageUrl,
                };
            },
        },
        pos: {
            comment: 'An animal\'s position, with some other data',
            type: DataTypes.VIRTUAL,
            get() {
                return {
                    id: this.id,
                    name: this.name,
                    imageUrl: this.imageUrl,
                    posX: this.posX,
                    posY: this.posY,
                };
            },
        },
    }, {
        sequelize,
        modelName: 'Animals',
    });
    return Animals;
};
'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Token extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.User);
        }
    }
    Token.init({
        id: {
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            comment: 'Token unique identifier',
            type: DataTypes.INTEGER,
        },
        token: {
            unique: true,
            comment: 'Token',
            type: DataTypes.STRING,
        },
        userId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            comment: 'User\'s id',
        },
    }, {
        sequelize,
        modelName: 'Token',
    });
    return Token;
};
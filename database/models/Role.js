'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Role extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.hasMany(models.User, { foreignKey: 'roleId' });
        }
    }
    Role.init({
        id: {
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            comment: 'Role unique identifier',
            type: DataTypes.INTEGER,
        },
        name: {
            comment: 'Role name',
            type: DataTypes.STRING,
            unique: true,
        },
    }, {
        sequelize,
        modelName: 'Role',
    });
    return Role;
};
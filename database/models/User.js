'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsToMany(models.Animals, { through: 'Favorites' });
            this.belongsTo(models.Role);
            this.hasOne(models.Token, { foreignKey: 'userId' });
        }
    }
    User.init({
        id: {
            primaryKey: true,
            autoIncrement: true,
            comment: 'User unique identifier',
            type: DataTypes.INTEGER,
        },
        login: {
            unique: true,
            comment: 'User\'s login',
            type: DataTypes.STRING,
        },
        passwd: {
            unique: true,
            comment: 'User\'s password',
            type: DataTypes.TEXT,
        },
        email: {
            unique: true,
            comment: 'User\'s email',
            type: DataTypes.STRING,
        },
        roleId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            comment: 'Role\'s id',
        },
        infos: {
            comment: 'Only necessary information\'s about a given user',
            type: DataTypes.VIRTUAL,
            get() {
                return {
                    id: this.id,
                    login: this.login,
                    email: this.email,
                    roleId: this.roleId,
                };
            },
        },
    }, {
        sequelize,
        modelName: 'User',
    });
    return User;
};
const dotenv = require('dotenv');

// Try to read the database config
const exec = dotenv.config();

// No reading errors allowed
if (exec.error) {
    process.exit(1);
}

module.exports = {
    'development': {
        'dialect': 'sqlite',
        'storage': 'database.db',
    },
    'test': {
        'username': 'root',
        'password': null,
        'database': 'database_test',
        'host': '127.0.0.1',
        'dialect': 'mysql',
    },
    'production': {
        'username': process.env.DB_USERNAME,
        'password': process.env.DB_PASSWORD,
        'database': process.env.DB_NAME,
        'host': process.env.DB_HOST,
        'port': process.env.DB_PORT,
        'dialect': process.env.DB_DIALECT,
    },
};

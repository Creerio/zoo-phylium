// Try to read the database config using dotenv
const dotenv = require('dotenv');
const exec = dotenv.config();

// No reading errors allowed
if (exec.error) {
    console.error('Unable to read environment variables. Exiting.');
    process.exit(1);
}

// Block server from starting, should no secret be given
if (!process.env.SECRET) {
    console.error('FATAL : A secret is required for the server to start !');
    console.error('FATAL : Please add one to your .env file.');
    console.error('FATAL : You may use this one for example => ' + require('crypto').randomBytes(64).toString('hex'));
    return;
}

// Load cleanup on stop
const nodeCleanup = require('node-cleanup');

// Load and connect to the database
const { databaseInstance } = require('./class/Database');
databaseInstance.createDatabaseConnector().then(() => {
    databaseInstance.connectToDB();

    // Database loaded, we can now launch express, etc.

    // Express server for the website
    const { app } = require('./src/server/server_base');

    // File & path to read js files
    const fs = require('fs');
    const path = require('path');

    // Load all paths
    const serverPaths = path.join(__dirname, './src/server/');
    const pathFiles = fs.readdirSync(serverPaths).filter(file => file.startsWith('path_') && file.endsWith('.js'));
    for (const file of pathFiles) {
        const filePath = path.join(serverPaths, file);
        require(filePath);
    }

    app.listen(process.env.PORT, () => {
        console.log(`App is launched ! Listening on port ${process.env.PORT} ! Try http://localhost:${process.env.PORT}`);
    });

    // Should the server be stopped, the database connection will be closed
    nodeCleanup(function() {
        databaseInstance.db.close();
    });
});


# Zoo Phylium

This is a zoo showcasing website made in NodeJs and React for our AAW class project.


# Installation and usage

## Requirements

* [NodeJs](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed on the system. We recommand the LTS version of [NodeJs](https://nodejs.org/), specifically 18.X, as it is what was used during development.

* A SQL database system, it must be operational before installation. [SQLite](https://sqlite.org/), [MySQL](https://www.mysql.com/) and [MariaDB](https://mariadb.org/) were used during development, but [PostgreSQL](https://www.postgresql.org/) should also be fine.


## Install

1. Clone this repository locally, and open a terminal in the folder

2. Run `npm i` to install all requiered dependencies

3. Depending on the database system you are going to use, install the requiered package to handle it. For example, you can use `npm i sqlite3` for [SQLite](https://sqlite.org/) or `npm i mysql2` for [MySQL](https://www.mysql.com/). See [this page](https://sequelize.org/docs/v6/getting-started/#installing) for more information

4. Since we have a database to import, update the [db_config.js](database/db_config.js) file accordingly to our database needs. You can find it under `database/db_config.js`. Because a .env file will be created later, you can use the following options :

  > For SQLite setups :

  ```
        'production': {
            'dialect': 'sqlite',
            'storage': 'database.db',
        },
  ```

  > Other setups :

  ```
        'production': {
            'username': process.env.DB_USERNAME,
            'password': process.env.DB_PASSWORD,
            'database': process.env.DB_NAME,
            'host': process.env.DB_HOST,
            'port': process.env.DB_PORT,
            'dialect': process.env.DB_DIALECT,
        },
  ```

5. From the [example](example) folder, copy [dotenv](example/dotenv) to the root of the repository. Rename it to `.env` and open it to modify at least everything related to the database (See the comments inside for more informations). Do not forget to uncomment the lines you are going to use !

6. Execute the following commands to have a working production database with default values :

  ```
      NODE_ENV=production npx sequelize-cli db:migrate
      NODE_ENV=production npx sequelize-cli db:seed:all
  ```

  If nothing happens on the database, or if you have one line indicating the version of Sequelize with nothing else, please verify your settings or enter the save command with the `--debug` argument added to it to fix the issue

7. Start the server in order to get a requiered value. You may do so with `node .`. Follow the given instructions

8. Done. You can now launch our project


## How to launch?

Launch two terminals, or use whatever you want, the idea is to be able to have two process running at the same time.
Run `npm run start-server` and `npm run start-web`, both must be running at the same time.
To access the website, use the link provided in the `npm run start-web` terminal.
Default accounts are available on the [default_users.js](database/seeders/default_users.js) seed file.

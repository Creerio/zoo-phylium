const bcrypt = require('bcrypt');
const { databaseInstance } = require('./Database');
const Users = databaseInstance.db.modelManager.getModel('User');
const Role = databaseInstance.db.modelManager.getModel('Role');

const jwt = require('jsonwebtoken');
const Tokens = databaseInstance.db.modelManager.getModel('Token');

/**
 * UserHandler, handles a user in the database with the required salt rounds
 */
class UserHandler {
    constructor() {
        // Store salt rounds for later, delete it from the stored env variables
        this.salt = process.env.SALT_ROUNDS ? process.env.SALT_ROUNDS : 10;
        delete process.env.SALT_ROUNDS;
    }

    /**
     * Adds a user to the database, this works almost ASYNCHRONOUSLY with no return value
     *
     * @param login
     * @param email
     * @param passwd
     * @param roleId
     */
    async addUser({ login, email, passwd, roleId }) {
        bcrypt.hash(passwd, this.salt, function(err, hash) {
            // Add only if the user doesn't already exist
            Users.findOrCreate({
                where: {
                    login: login,
                    email: email,
                },
                defaults: {
                    passwd: hash,
                    roleId: roleId,
                },
            });
        });
    }

    /**
     * Gives the user's instance using a token
     *
     * @param tkn
     * Token to use to get the user
     *
     * @returns {Promise<Model<TModelAttributes, TCreationAttributes>>}
     * User instance
     */
    async getUser(tkn) {
        const token = await Tokens.findOne({
            where: {
                token: tkn,
            },
        });

        return await Users.findOne({
            where: {
                id: token.userId,
            },
        });
    }

    /**
     * Updates a stored user in the database
     * @param user
     * @param login
     * @param email
     * @param passwd
     * @returns {Promise<void>}
     */
    async updateUser(user, { login, email, passwd }) {
        bcrypt.hash(passwd, this.salt, async function(err, hash) {
            user.set({
                login: login,
                passwd: hash,
                email: email,
            });

            await user.save();
        });
    }

    /**
     * Disconnects a user
     *
     * @param userId
     * User's id
     *
     * @returns {Promise<void>}
     */
    async disconnectUser(userId) {
        await Tokens.destroy({
            where: {
                userId: userId,
            },
        });
    }

    /**
     * Checks if a given user's password is the same as the stored one using bcrypt
     *
     * @param userObj
     * User from the database
     *
     * @param passwd
     * Password given from the front end
     *
     * @returns {Promise<string>}
     */
    async checkPasswd(userObj, passwd) {
        return bcrypt.compare(passwd, userObj.passwd);
    }

    /**
     * Checks if the current role is the same as the requiered one
     *
     * @param userObj
     * User
     *
     * @param roleName
     * Role name
     *
     * @returns {Promise<boolean>}
     */
    async checkRole(userObj, roleName) {
        const role = await Role.findOne({
            where: {
                name: roleName,
            },
        });

        return userObj.roleId === role.id;
    }

    /**
     * Adds a token to the database for a given user
     *
     * @param userObj
     * The user which will receive the token
     *
     * @returns {*}
     * Returns the created token
     */
    async addToken(userObj) {
        const token = await Tokens.findOne({
            where: {
                userId: userObj.id,
            },
        });

        // Destroy any already existing token for this user, they have one instance and nothing else
        if (token) {
            await token.destroy();
        }

        const createdToken = jwt.sign({
            id: userObj.id,
        }, process.env.SECRET, { expiresIn: '3h' });

        Tokens.create({
            token: createdToken,
            userId: userObj.id,
        });

        return createdToken;
    }

    /**
     * Checks if the given token is valid for the user
     *
     * @param token
     * Token to analyse
     *
     * @returns {Promise<boolean>}
     * True if
     */
    async checkToken(token) {
        const tokenObj = await Tokens.findOne({
            where: {
                token: token,
            },
        });

        if (!tokenObj) {
            return false;
        }
        else {
            return jwt.verify(token, process.env.SECRET, function(err) {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        tokenObj.destroy();
                    }
                    return false;
                }
                return true;
            });
        }
    }
}

// Singleton instance
const userChecker = new UserHandler();

module.exports = { userHandler: userChecker };
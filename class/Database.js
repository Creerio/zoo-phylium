const { Sequelize } = require('sequelize');
const fs = require('fs');
const path = require('path');

// Fix PG date handling
try {
    const pg = require('pg');
    pg.types.setTypeParser(1114, function(stringValue) {
        return new Date(stringValue.substring(0, 10) + 'T' + stringValue.substring(11) + 'Z');
    });
    console.debug('Postgres is installed, fixed the date handling');
}
catch (e) {
    console.debug('Postgres is not installed, ignoring date workaround');
}

/**
 * Database
 */
class Database {
    constructor() {
        this.db = null;
    }

    /**
     * Creates the database connector for the program
     */
    async createDatabaseConnector() {
        // Sqlite used or nothing? Need some specific information
        if (process.env.DB_DIALECT === 'sqlite' || typeof (process.env.DB_DIALECT) != 'string') {
            this.db = new Sequelize({
                dialect: 'sqlite',
                storage: process.env.DB_PATH ? process.env.DB_PATH : 'database.db', // Use database.db as default storage for the database in case it's not defined
                logging: msg => console.debug(msg),
            });
        }
        else {
            if (!process.env.DB_HOST || !process.env.DB_PORT || !process.env.DB_NAME || !process.env.DB_USERNAME || (!process.env.DB_PASSWORD && process.env.DB_PASSWORD !== '')) {
                console.error('Missing some database information. To use a non sqlite database, you require a host (ip), port, username, password and the database name. See the .env file');
                process.exit(2);
            }
            // Use info to create database connector
            this.db = new Sequelize({
                dialect: process.env.DB_DIALECT,
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
                host: process.env.DB_HOST,
                port: process.env.DB_PORT,
                logging: msg => console.debug(msg),
            });
        }

        // Store models
        let testTables;
        for (const model of Object.keys(getModelsFromFolder(this.db))) {
            this.db.modelManager.addModel(model);
            testTables = this.db.modelManager.getModel(model);
            try {
                testTables = await testTables.findAll();
            }
            catch (err) {
                console.error('There was an error while getting all tables, some may be missing from the database. Please check this using sequelize-cli.');
                process.exit(3);
            }
        }

    }

    /**
     * Connect to the database
     */
    connectToDB() {
        this.db.authenticate()
            .then(() => {
                console.info('Connected to the registered database');
            })
            .catch(err => {
                console.error('Unable to connect to the database, see the error bellow for more information');
                console.error(err);
                process.exit(4);
            });
    }
}

/**
 * Returns all models from the database/models folder
 *
 * @param sequelizeInstance
 * The sequelize instance
 *
 * @returns {{}}
 */
function getModelsFromFolder(sequelizeInstance) {
    const models = {};
    fs
        .readdirSync(__dirname + '/../database/models')
        .filter(file => {
            return (file.indexOf('.') !== 0) && (!file.endsWith('index.js')) && (file.slice(-3) === '.js');
        })
        .forEach(file => {
            const model = require(path.join(__dirname + '/../database/models/', file))(sequelizeInstance, require('sequelize').DataTypes);
            models[model.name] = model;
        });
    Object.keys(models).forEach(modelName => {
        if (models[modelName].associate) {
            models[modelName].associate(models);
        }
    });
    return models;
}

// Singleton object
const databaseInstance = new Database();

// Export the logger
module.exports = { databaseInstance, getModelsFromFolder };

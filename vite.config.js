import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// Try to read the config using dotenv
const dotenv = require('dotenv');
const exec = dotenv.config();

// No reading errors allowed
if (exec.error) {
  console.error('Unable to read environment variables. Exiting.');
  process.exit(1);
}

// Remove unneeded environment variables, like the SECRET
delete process.env.SECRET;
delete process.env.SALT_ROUNDS;

// Use given server port
const port = process.env.PORT ? process.env.PORT : 8080 ;

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:' + port,
      },
      '/images': {
        target: 'http://localhost:' + port,
      },
    }
  }
})
